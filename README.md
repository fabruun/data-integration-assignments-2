# Data Integration - Assignments 2

## Data Integration - Assignment 2.0

Research and answer the following: 

1. Are maxLength, or minLength allowed on optional name/value pairs?
2. Based on the basics from [Chapter 40](http://x15.dk/webdev/site/ch40.xhtml) JSON has Number, Boolean, Null, String, Object, and Array as object types. Research whether they all apply as types in JSON-schema. Make an example that proves your answer. 
3. Make a JSON-schema for the JSON [in the section called "Assignment DI.1.0"](http://x15.dk/webdev/site/ch40s09.xhtml#assDI10)  and perform validation of the JSON 
4. Make a JSON-schema for the JSON [in the section called "Assignment DI.1.1"](http://x15.dk/webdev/site/ch40s09.xhtml#assDI11) and perform validation of the JSON 
5. Make a JSON-schema for the JSON [in the section called "Assignment DI.1.2"](http://x15.dk/webdev/site/ch40s09.xhtml#assDI12) and perform validation of the JSON 

## Data Integration - Assignment 2.1

Create an AJaX one page application that allows chosing a country from a drop down or as a key in. The page issues an AJaX request to a PHP program reading from the World database. The data must be returned as JSON and displayed by JavaScript. The displayed data must be: 

* Country name
* Population
* Capital city
* Population of capital city
* All official languages
* Density of population
* Head of state

## Data Integration - Assignment 2.2

Create an AJaX one page application that allows chosing a country, district combination from the World db. The page issues an AJaX request to a PHP program reading from the database. The data must be returned as JSON and displayed by JavaScript. The displayed data must be: 

* Country name
* District name
* Cities from district
* Population of said cities
* Population of district
* Population of country
