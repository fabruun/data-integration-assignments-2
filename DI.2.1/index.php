<?php
require_once 'db/DbP.inc.php';
require_once 'db/DbH.inc.php';
$dbh = DbH::getDbH();
$sql = "select
country.name AS 'countryName',
country.population AS 'countryPopulation',
city.name 'capitalName',
city.population AS 'cityPopulation',
countrylanguage.language AS 'language',
country.population/country.surfacearea AS 'density',
country.headofstate AS 'headOfState'";
$sql .= " from country";
$sql .= " join city on country.code=city.countrycode";
$sql .= " join countrylanguage on country.code=countrylanguage.countrycode";
$sql .= " group by country.name";
$res = $dbh->query($sql);

$a = array();
while ($out = $res->fetch(PDO::FETCH_ASSOC)) {
    array_push($a, $out);
}

$content = json_encode($a);
file_put_contents('countries.json', $content);
require_once('content.php');
?>
