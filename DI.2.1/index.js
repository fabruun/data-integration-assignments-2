createButton = function() {

  let h1 = document.createElement('h1');
  let h1text = document.createTextNode('Counties');
  h1.appendChild(h1text);
  document.body.appendChild(h1);

  let content = document.createElement('select');
  document.body.appendChild(content);
  content.setAttribute('id', 'countries');

  let submit = document.createElement('input');
  document.body.appendChild(submit);
  submit.setAttribute('type', 'submit');
  submit.setAttribute('value', 'Display country');
  submit.setAttribute('id', 'submitButton');
  document.getElementById('submitButton').style.marginTop = "2em";

  let countryInfo = document.createElement('div');
  document.body.appendChild(countryInfo);
  countryInfo.setAttribute('id', 'displayCountry');
}


window.addEventListener('load', createButton);



function loadCountries() {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', 'countries.json', true);

    xhr.onload = function () {
        if (this.status === 200) {
            let countries = JSON.parse(this.responseText);
            let output = '';
            for (let i in countries)
                output += '<option>'+countries[i].countryName+'</option>';



            document.getElementById('countries').innerHTML = output;
            document.getElementById('countries').style.display = "block";
        }
        else {
            document.getElementById('countries').innerHTML = "Not found";
        }
    }

    xhr.onerror = function () {
        document.getElementById('countries').innerHTML = "Request error";
    }
    xhr.send();
}

countryInfo = function () {
  let xhr = new XMLHttpRequest();
  xhr.open('GET', 'countries.json', true);

  xhr.onload = function () {
      if (this.status === 200) {
          let countries = JSON.parse(this.responseText);
          let output = "";
          for(let i in countries)
              output += "<ul>" +
                  "<li><b>Country Name: </b> "+countries[i].countryName+"</li>" +
                  "<li><b>Country Population: </b>"+countries[i].countryPopulation+"</li>" +
                  "<li><b>Capital Name: </b>"+countries[i].capitalName+"</li>" +
                  "<li><b>Capital Population</b>"+countries[i].cityPopulation+"</li>" +
                  "<li><b>Country Density: </b>"+countries[i].density+"</li>" +
                  "<li><b>Head of State: </b>"+countries[i].headOfState+"</li>" +
                  "</ul>";



          document.getElementById('displayCountry').innerHTML = output;
          document.getElementById('displayCountry').style.display = "block";
      }
      else {
          document.getElementById('countries').innerHTML = "Not found";
      }
  }

  xhr.onerror = function () {
      document.getElementById('countries').innerHTML = "Request error";
  }
  xhr.send();
}

getCountry = function() {
  document.getElementById('submitButton').addEventListener('click', countryInfo);
}

window.addEventListener('load', loadCountries);
window.addEventListener('load', getCountry);
