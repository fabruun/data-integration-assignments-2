select
country.name AS 'Country name',
country.population AS 'Country Population',
city.name 'Capital name',
city.population AS 'Capital Population',
countrylanguage.language AS 'Languages',
country.population/country.surfacearea AS 'Density of population',
country.headofstate AS 'Head of state'
from country
join city on country.code=city.countrycode
join countrylanguage on country.code=countrylanguage.countrycode
group by country.name;