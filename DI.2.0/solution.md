

  1. Are maxLength, or minLength allowed on optional name/value pairs?
yes they are allowed. See folder: question1

  2. Based on the basics from Chapter 40 JSON has Number, Boolean, Null, String, Object, and Array as object types. Research whether they all apply as types in JSON-schema. Make an example that proves your answer.
  yes they are all allowed. See folder: question2


  3. Make a JSON-schema for the JSON in the section called “Assignment DI.1.0” and perform validation of the JSON
  See folder: question3

  4. Make a JSON-schema for the JSON in the section called “Assignment DI.1.1” and perform validation of the JSON

  5. Make a JSON-schema for the JSON in the section called “Assignment DI.1.2” and perform validation of the JSON
